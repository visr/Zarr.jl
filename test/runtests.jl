using Test
using Zarr
using JSON

@testset "Zarr" begin

@testset "Metadata" begin
    @testset "Data type encoding" begin
        @test Zarr.typestr(Bool) === "<b1"
        @test Zarr.typestr(Int8) === "<i1"
        @test Zarr.typestr(Int64) === "<i8"
        @test Zarr.typestr(UInt32) === "<u4"
        @test Zarr.typestr(UInt128) === "<u16"
        @test Zarr.typestr(Complex{Float32}) === "<c8"
        @test Zarr.typestr(Complex{Float64}) === "<c16"
        @test Zarr.typestr(Float16) === "<f2"
        @test Zarr.typestr(Float64) === "<f8"
    end

    @testset "Metadata struct and JSON representation" begin
        A = fill(1.0, 30, 20)
        chunks = (5,10)
        metadata = Zarr.Metadata(A, chunks; fill_value=-1.5)
        @test metadata isa Zarr.Metadata
        @test metadata.zarr_format === 2
        @test metadata.shape === size(A)
        @test metadata.chunks === chunks
        @test metadata.dtype === "<f8"
        @test metadata.compressor === nothing
        @test metadata.fill_value === -1.5
        @test metadata.order === 'F'
        @test metadata.filters === nothing

        jsonstr = json(metadata)
        metadata_cycled = Zarr.Metadata(jsonstr)
        @test metadata === metadata_cycled
    end

    @testset "Fill value" begin
        @test Zarr.fill_value_encoding(Inf) === "Infinity"
        @test Zarr.fill_value_encoding(-Inf) === "-Infinity"
        @test Zarr.fill_value_encoding(NaN) === "NaN"
        @test Zarr.fill_value_encoding(nothing) === nothing
        @test Zarr.fill_value_encoding("-") === "-"

        @test Zarr.fill_value_decoding("Infinity", Float64) === Inf
        @test Zarr.fill_value_decoding("-Infinity", Float64) === -Inf
        @test Zarr.fill_value_decoding("NaN", Float32) === NaN32
        @test Zarr.fill_value_decoding("3.4", Float64) === 3.4
        @test Zarr.fill_value_decoding("3", Int) === 3
        @test Zarr.fill_value_decoding(nothing, Int) === nothing
        @test Zarr.fill_value_decoding("-", String) === "-"
    end
end

@testset "Path" begin
    @test Zarr.normalize_path(".\\\\path///to\\a\\place/..\\///") == "path/to/a"
end

end  # @testset "Zarr"

#= 
# Below is just interactively exploring how to implement chunks,
# possibly using Dagger.

# Spec example 1 but in memory
# https://zarr.readthedocs.io/en/stable/spec/v2.html#storing-a-single-array
using Dagger

A = fill(Int32(42), (20, 20))

chunks = (10, 10)
metadata = Zarr.Metadata(A, chunks; fill_value=Int32(42))

attrs = Dict{String, Any}(
    "foo" => 42,
    "bar" => "apples",
    "baz" => [1, 2, 3, 4])

store = Dict{String, Any}()
# no need to convert to JSON if it is in memory
store[".zarray"] = metadata
store[".zattrs"] = attrs


Blocks
Dagger.DArray

Blocks(2,2)
rand(Blocks(2,2), 3,3)
x = compute(rand(Blocks(2,2), 3,3))
collect(x)

a = rand(Blocks(10), 20)
compute(a)
collect(compute(a))
collect(a)
Dagger.chunks(compute(a))

Distribute(Blocks(10), a)
c = Dagger.chunks(compute(a))[1]

# Chunk of MemPool.DRef should be MemPool.FileRef
c.handle
c.handle.where
compute(a)

collect(c)

Dagger.savechunk(rand(3), "d", "d01")

Dagger.save(c, "dag-c1.bin")
open("dag.bin", "w") do io
    Dagger.save(c, io, compute(a))
end
=#
