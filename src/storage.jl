# do we need a Store type or can we just use AbstractDict?
abstract type Store{K,V} <: AbstractDict{K,V} end

struct DirectoryStore{K,V} <: Store{K,V}
    dir::String
end

struct DictStore{K,V} <: Store{K,V}
    dir::String
end
