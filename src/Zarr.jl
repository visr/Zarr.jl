module Zarr

using JSON

include("metadata.jl")
include("storage.jl")
include("array.jl")

key(chunk::NTuple{N, Int} where {N}) = join(chunk, '.')

function normalize_path(p::AbstractString)
    p = normpath(p)
    p = replace(p, '\\'=>'/')
    strip(p, '/')
end

end  # module Zarr
