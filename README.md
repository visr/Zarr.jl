# Zarr.jl

This package has been integrated into https://github.com/meggart/ZarrNative.jl,
and development will continue there.

## Links
https://discourse.julialang.org/t/a-julia-compatible-alternative-to-zarr/11842
https://github.com/zarr-developers/zarr/issues/284
https://zarr.readthedocs.io/en/stable/spec/v2.html

## Potentially useful packages
https://github.com/JuliaArrays/TiledIteration.jl
https://github.com/JuliaArrays/BlockArrays.jl
https://github.com/JuliaParallel/Blocks.jl
https://github.com/JuliaParallel/DistributedArrays.jl
https://github.com/JuliaParallel/Dagger.jl
How do Dagger.DArray and DistributedArrays.Darray compare?
https://github.com/seung-lab/BigArrays.jl
https://github.com/bicycle1885/TranscodingStreams.jl

## Supported

## Not yet supported

- Compression
- "C" row-major order
- Filters

### Data types
- "m": Timedelta
- "M": Datetime
- "S": string (fixed-length sequence of char)
- "U": unicode (fixed-length sequence of Py_UNICODE)
- Structured data types (i.e., with multiple named fields)

## Assumptions
- Everything is little-endian

## Remember

## TODO
